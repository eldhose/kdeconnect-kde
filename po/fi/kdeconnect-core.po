# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tommi Nieminen <translator@legisign.org>, 2014, 2018, 2019, 2020, 2021.
# Lasse Liehu <lasse.liehu@gmail.com>, 2014, 2015, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-03-12 20:22+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: backends/bluetooth/bluetoothpairinghandler.cpp:56
#: backends/lan/lanpairinghandler.cpp:52
#, kde-format
msgid "Canceled by other peer"
msgstr "Vertaiskäyttäjä perui"

#: backends/bluetooth/bluetoothpairinghandler.cpp:65
#: backends/lan/lanpairinghandler.cpp:61
#, kde-format
msgid "%1: Already paired"
msgstr "%1: Pari on jo muodostettu"

#: backends/bluetooth/bluetoothpairinghandler.cpp:68
#, kde-format
msgid "%1: Pairing already requested for this device"
msgstr "%1: Tälle laitteelle on jo pyydetty paria"

#: backends/bluetooth/bluetoothpairinghandler.cpp:124
#: backends/lan/lanpairinghandler.cpp:106
#, kde-format
msgid "Timed out"
msgstr "Aikakatkaisu"

#: backends/lan/compositeuploadjob.cpp:82
#, kde-format
msgid "Couldn't find an available port"
msgstr "Käytettävissä olevaa porttia ei löydetty"

#: backends/lan/compositeuploadjob.cpp:121
#, kde-format
msgid "Failed to send packet to %1"
msgstr "Paketin lähetys kohteeseen %1 epäonnistui"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "Sending to %1"
msgstr "Lähetetään kohteeseen %1"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "File"
msgstr "Tiedosto"

#: backends/lan/landevicelink.cpp:146 backends/lan/landevicelink.cpp:160
#, kde-format
msgid ""
"This device cannot be paired because it is running an old version of KDE "
"Connect."
msgstr ""
"Tätä laitetta ei voi yhdistää pariksi, koska se käyttää vanhaa versiota KDE "
"Connectista."

#: compositefiletransferjob.cpp:46
#, kde-format
msgctxt "@title job"
msgid "Receiving file"
msgid_plural "Receiving files"
msgstr[0] "Vastaanotetaan tiedostoa"
msgstr[1] "Vastaanotetaan tiedostoja"

#: compositefiletransferjob.cpp:47
#, kde-format
msgctxt "The source of a file operation"
msgid "Source"
msgstr "Lähde"

#: compositefiletransferjob.cpp:48
#, kde-format
msgctxt "The destination of a file operation"
msgid "Destination"
msgstr "Kohde"

#: device.cpp:215
#, kde-format
msgid "Already paired"
msgstr "Pari on jo muodostettu"

#: device.cpp:220
#, kde-format
msgid "Device not reachable"
msgstr "Laite tavoittamattomissa"

#: device.cpp:561
#, kde-format
msgid "SHA256 fingerprint of your device certificate is: %1\n"
msgstr "Laitteen varmenteen SHA256-sormenjälki on: %1\n"

#: device.cpp:569
#, kde-format
msgid "SHA256 fingerprint of remote device certificate is: %1\n"
msgstr "Etälaitteen varmenteen SHA256-sormenjälki on: %1\n"

#: filetransferjob.cpp:51
#, kde-format
msgid "Filename already present"
msgstr "Tiedostonimi on jo olemassa"

#: filetransferjob.cpp:100
#, kde-format
msgid "Received incomplete file: %1"
msgstr "Vastaanotettiin epätäydellinen tiedosto: %1"

#: filetransferjob.cpp:118
#, kde-format
msgid "Received incomplete file from: %1"
msgstr "Vastaanotettiin epätäydellinen lähettäjältä: %1"

#: kdeconnectconfig.cpp:76
#, kde-format
msgid "KDE Connect failed to start"
msgstr "KDE Connectin käynnistyminen epäonnistui"

#: kdeconnectconfig.cpp:77
#, kde-format
msgid ""
"Could not find support for RSA in your QCA installation. If your "
"distribution provides separate packets for QCA-ossl and QCA-gnupg, make sure "
"you have them installed and try again."
msgstr ""
"QCA-asennuksestasi ei löytynyt RSA-tukea. Jos jakelusi tarjoaa erilliset QCA-"
"ossl- ja QCA-gnupg-paketit, tarkista, että ne on asennettu, ja yritä "
"uudelleen."

#: kdeconnectconfig.cpp:313
#, kde-format
msgid "Could not store private key file: %1"
msgstr "Ei voitu tallentaa yksityisen avaimen tiedostoa: %1"

#: kdeconnectconfig.cpp:358
#, kde-format
msgid "Could not store certificate file: %1"
msgstr "Ei voitu tallentaa vamennetiedostoa: %1"

#~ msgid "Sent 1 file"
#~ msgid_plural "Sent %1 files"
#~ msgstr[0] "Lähetettiin 1 tiedosto"
#~ msgstr[1] "Lähetettiin %1 tiedostoa"

#~ msgid "Progress"
#~ msgstr "Edistyminen"

#~ msgid "Sending file %1 of %2"
#~ msgstr "Lähetetään tiedostoa %1 / %2"

#~ msgid "Receiving file %1 of %2"
#~ msgstr "Vastaanotetaan tiedostoa %1 / %2"

#~ msgid "Receiving file over KDE Connect"
#~ msgstr "Vastaanotetaan tiedostoa KDE Connectilla"

#~ msgctxt "File transfer origin"
#~ msgid "From"
#~ msgstr "Lähde"

#~ msgctxt "File transfer destination"
#~ msgid "To"
#~ msgstr "Kohde"

#~ msgid "Finished sending to %1"
#~ msgstr "Lähetys kohteeseen %1 valmis"

#~ msgid "Error contacting device"
#~ msgstr "Virhe yhdistettäessä laitteeseen"

#~ msgid "Received incorrect key"
#~ msgstr "Vastaanotettiin väärä avain"

#~ msgid "Canceled by the user"
#~ msgstr "Käyttäjä perui"

#~ msgid "Pairing request from %1"
#~ msgstr "Paripyyntö laitteelta %1"

#~ msgid "Accept"
#~ msgstr "Hyväksy"

#~ msgid "Reject"
#~ msgstr "Hylkää"

#~ msgid "Incoming file exists"
#~ msgstr "Saapuva tiedosto on jo olemassa"

#~ msgctxt "Device name that will appear on the jobs"
#~ msgid "KDE-Connect"
#~ msgstr "KDE-Connect"
